%module(directors="1") fuzzylite
%feature("autodoc", "3");

%{

#include <fl/Headers.h>

%}

// MAKE SURE C++ EXCEPTIONS DONT CRASH PYTHON

%include exception.i

%exception {
  try {
    $action
  } catch (const std::exception& e) {
    SWIG_exception(SWIG_RuntimeError, e.what());
  }
}

// INCLUDES

%include std_except.i
%include std_vector.i
%include std_string.i
%include attribute.i
%include operators_python.i

%include <fl/fuzzylite.h>

// hedge

%include <fl/hedge/Hedge.h>

%include <fl/hedge/Any.h>
%include <fl/hedge/Extremely.h>
%include <fl/hedge/Not.h>
%include <fl/hedge/Seldom.h>
%include <fl/hedge/Somewhat.h>
%include <fl/hedge/Very.h>

// imex

%newobject fromString;

%include <fl/imex/Importer.h>
%include <fl/imex/Exporter.h>

%include <fl/imex/CppExporter.h>
%include <fl/imex/FclExporter.h>
%include <fl/imex/FclImporter.h>
%include <fl/imex/FisExporter.h>
%include <fl/imex/FisImporter.h>
%include <fl/imex/FldExporter.h>
%include <fl/imex/FllExporter.h>
%include <fl/imex/FllImporter.h>
%include <fl/imex/JavaExporter.h>

namespace fl {
	struct RuleVector;
}

%template(RuleMatrix) std::vector<fl::RuleVector>;
%template(intVector) std::vector<int>;

%newobject toMatrix;

%include <fl/imex/RuleMatrixConvert.h>

%feature("python:slot", "tp_str", functype="reprfunc") fl::RuleVector::__to_string__;
%feature("python:slot", "tp_repr", functype="reprfunc") fl::RuleVector::__to_string__;

%extend fl::RuleVector {

	std::string __to_string__() {
		std::ostringstream out;
		
		for(unsigned int i = 0; i < $self->antecedent.size(); i++) {
			out << $self->antecedent[i] << " ";
		}

		out << ", ";

		for(unsigned int i = 0; i < $self->consequent.size(); i++) {
			out << $self->consequent[i] << " ";
		}

		out << "(" << $self->weight << ") : " << $self->op;

		return out.str();
	}

}

// norm

%include <fl/norm/Norm.h>
%include <fl/norm/SNorm.h>
%include <fl/norm/TNorm.h>

%include <fl/norm/s/AlgebraicSum.h>
%include <fl/norm/s/BoundedSum.h>
%include <fl/norm/s/DrasticSum.h>
%include <fl/norm/s/EinsteinSum.h>
%include <fl/norm/s/HamacherSum.h>
%include <fl/norm/s/Maximum.h>
%include <fl/norm/s/NormalizedSum.h>

%include <fl/norm/t/AlgebraicProduct.h>
%include <fl/norm/t/BoundedDifference.h>
%include <fl/norm/t/DrasticProduct.h>
%include <fl/norm/t/EinsteinProduct.h>
%include <fl/norm/t/HamacherProduct.h>
%include <fl/norm/t/Minimum.h>

// rule

/*
%template(RuleBlockVector) std::vector<fl::RuleBlock*>;
%template(PropositionVector) std::vector<fl::Proposition*>;

%attribute2ref(fl::Engine, const std::vector<fl::RuleBlock*>, ruleBlocks, ruleBlocks);
%attribute2ref(fl::Consequent, const std::vector<fl::Proposition*>, conclusions, conclusions);
*/

%include <fl/rule/Antecedent.h>
%include <fl/rule/Consequent.h>
%include <fl/rule/Expression.h>
%include <fl/rule/Rule.h>
%include <fl/rule/RuleBlock.h>

// term

%include <fl/term/Term.h>
%include <fl/term/Accumulated.h>
%include <fl/term/Bell.h>
%include <fl/term/Constant.h>
%include <fl/term/Discrete.h>
%include <fl/term/Function.h>
%include <fl/term/Gaussian.h>
%include <fl/term/GaussianProduct.h>
%include <fl/term/Linear.h>
%include <fl/term/PiShape.h>
%include <fl/term/Ramp.h>
%include <fl/term/Rectangle.h>
%include <fl/term/Sigmoid.h>
%include <fl/term/SigmoidDifference.h>
%include <fl/term/SigmoidProduct.h>
%include <fl/term/SShape.h>
%include <fl/term/Thresholded.h>
%include <fl/term/Trapezoid.h>
%include <fl/term/Triangle.h>
%include <fl/term/ZShape.h>

// variable

%include <fl/variable/Variable.h>
%include <fl/variable/InputVariable.h>
%include <fl/variable/OutputVariable.h>

// defuzzifier

%include <fl/defuzzifier/Defuzzifier.h>
%include <fl/defuzzifier/IntegralDefuzzifier.h>
%include <fl/defuzzifier/Bisector.h>
%include <fl/defuzzifier/Centroid.h>
%include <fl/defuzzifier/LargestOfMaximum.h>
%include <fl/defuzzifier/MeanOfMaximum.h>
%include <fl/defuzzifier/SmallestOfMaximum.h>
%include <fl/defuzzifier/Tsukamoto.h>
%include <fl/defuzzifier/WeightedAverage.h>
%include <fl/defuzzifier/WeightedSum.h>

// factory

%include <fl/factory/Factory.h>

namespace fl {

%template(DefuzzifierFactoryTpl) Factory<Defuzzifier*>;
%template(HedgeFactoryFactoryTpl) Factory<Hedge*>;
%template(SNormFactoryTpl) Factory<SNorm*>;
%template(TermFactoryTpl) Factory<Term*>;
%template(TNormFactoryTpl) Factory<TNorm*>;

}

%include <fl/factory/FactoryManager.h>

%include <fl/factory/DefuzzifierFactory.h>
%include <fl/factory/HedgeFactory.h>
%include <fl/factory/SNormFactory.h>
%include <fl/factory/TermFactory.h>
%include <fl/factory/TNormFactory.h>

// main stuff

namespace std {
	%template(scalarVector) vector<fl::scalar>;
	%template(InputVariableVector) std::vector<fl::InputVariable*>;
	%template(OutputVariableVector) std::vector<fl::OutputVariable*>;
	%template(TermVector) std::vector<fl::Term*>;
};

%attribute2ref(fl::Engine, const std::vector<fl::InputVariable*>, inputVariables, inputVariables);
%attribute2ref(fl::Engine, const std::vector<fl::OutputVariable*>, outputVariables, outputVariables);
%attribute2ref(fl::InputVariable, const std::vector<fl::Term*>, terms, terms);
%attribute2ref(fl::OutputVariable, const std::vector<fl::Term*>, terms, terms);

%include <fl/Engine.h>
%include <fl/Exception.h>
%include <fl/Operation.h>

