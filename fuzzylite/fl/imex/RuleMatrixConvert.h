/*   Copyright 2013 Juan Rada-Vilela

 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at

 http://www.apache.org/licenses/LICENSE-2.0

 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
 */

#ifndef FL_RULEMATRIXCONVERT_H
#define	FL_RULEMATRIXCONVERT_H

#include "fl/fuzzylite.h"
#include <vector>

namespace fl {

	class Engine;
    class Rule;
	class RuleBlock;
    class Proposition;
    class Variable;

	struct RuleVector {
		std::vector<int> antecedent;
		std::vector<int> consequent;
		scalar weight;
		short op;
	};

	//! Converts Engine's rules to and from a Matlab-like FIS rule matrix structure.
    class FL_EXPORT RuleMatrixConvert {
    protected:
        std::vector<int> translate(const std::vector<Proposition*>& propositions,
            const std::vector<Variable*> variables) const;
        RuleVector exportRule(const Rule* rule, const Engine* engine) const;

        void importRules(const std::vector<RuleVector>& ruleVector, Engine* engine) const;
        std::string translateProposition(scalar code, Variable* variable) const;
        
    public:
		std::vector<RuleVector> toMatrix(const RuleBlock* ruleBlock, const Engine* engine) const;
		void fromMatrix(RuleBlock* ruleBlock, const Engine* engine, std::vector<RuleVector> ruleMatrix) const;
    };

} // namespace fl

#endif	/* FL_RULEMATRIXCONVERT_H */

