/*   Copyright 2013 Juan Rada-Vilela

 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at

 http://www.apache.org/licenses/LICENSE-2.0

 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
 */

#include "fl/imex/RuleMatrixConvert.h"

#include "fl/Headers.h"
#include <queue>

namespace fl {

	std::vector<RuleVector> RuleMatrixConvert::toMatrix(const RuleBlock* ruleBlock, const Engine* engine) const {
		std::vector<RuleVector> ruleMatrix;

        for (int ixRule = 0; ixRule < ruleBlock->numberOfRules(); ++ixRule) {
            ruleMatrix.push_back(exportRule(ruleBlock->getRule(ixRule), engine));
        }

        return ruleMatrix;
	}

    RuleVector RuleMatrixConvert::exportRule(const Rule* rule, const Engine* engine) const {
        if (not rule) return RuleVector();

        std::vector<Proposition*> propositions;
        std::vector<Operator*> operators;

        std::queue<Expression*> bfsQueue;
        bfsQueue.push(rule->getAntecedent()->getRoot());
        while (not bfsQueue.empty()) {
            Expression* front = bfsQueue.front();
            bfsQueue.pop();
            if (front->isOperator) {
                Operator* op = dynamic_cast<Operator*> (front);
                bfsQueue.push(op->left);
                bfsQueue.push(op->right);
                operators.push_back(op);
            } else {
                propositions.push_back(dynamic_cast<Proposition*> (front));
            }
        }

        bool equalOperators = true;
        for (std::size_t i = 0; i + 1 < operators.size(); ++i) {
            if (operators.at(i)->name != operators.at(i + 1)->name) {
                equalOperators = false;
                break;
            }
        }
        if (not equalOperators) {
            throw fl::Exception("[exporter error] "
                    "RuleMatrixConvert does not support rules with different connectors "
                    "(i.e. ['and', 'or']). All connectors within a rule must be the same", FL_AT);
        }

		RuleVector ruleVector;

        std::vector<Variable*> inputVariables, outputVariables;
        for (int i = 0; i < engine->numberOfInputVariables(); ++i)
            inputVariables.push_back(engine->getInputVariable(i));
        for (int i = 0; i < engine->numberOfOutputVariables(); ++i)
            outputVariables.push_back(engine->getOutputVariable(i));

        ruleVector.antecedent = translate(propositions, inputVariables);
        ruleVector.consequent = translate(rule->getConsequent()->conclusions(), outputVariables);

		ruleVector.weight = rule->getWeight();

        if (operators.size() == 0) ruleVector.op = 1; //does not matter
        else {
            if (operators.at(0)->name == Rule::FL_AND) ruleVector.op = 1;
            else if (operators.at(0)->name == Rule::FL_OR) ruleVector.op = 2;
            else throw fl::Exception("[exporter error] "
                    "RuleMatrixConvert does not support rules connectors other than"
                    "FL_AND and FL_OR.", FL_AT);
        }

        return ruleVector;
    }

    std::vector<int> RuleMatrixConvert::translate(const std::vector<Proposition*>& propositions,
            const std::vector<Variable*> variables) const {
		std::vector<int> rulePart;

        for (std::size_t ixVariable = 0; ixVariable < variables.size(); ++ixVariable) {
            Variable* variable = variables.at(ixVariable);
            int termIndexPlusOne = 0;
            scalar plusHedge = 0;
            int negated = 1;
            for (std::size_t ixProposition = 0; ixProposition < propositions.size(); ++ixProposition) {
                Proposition* proposition = propositions.at(ixProposition);
                if (proposition->variable != variable) continue;

                for (int termIndex = 0; termIndex < variable->numberOfTerms(); ++termIndex) {
                    if (variable->getTerm(termIndex) == proposition->term) {
                        termIndexPlusOne = termIndex + 1;
                        break;
                    }
                }

                std::vector<Hedge*> hedges = proposition->hedges;
                if (hedges.size() > 1) {
                    FL_DBG("[exporter warning] only a few combinations of multiple "
                            "hedges are supported in fis files");
                }
                for (std::size_t ixHedge = 0; ixHedge < hedges.size(); ++ixHedge) {
                    Hedge* hedge = hedges.at(ixHedge);
                    if (hedge->name() == Not().name()) negated *= -1;
                    else if (hedge->name() == Extremely().name()) plusHedge += 0.3;
                    else if (hedge->name() == Very().name()) plusHedge += 0.2;
                    else if (hedge->name() == Somewhat().name()) plusHedge += 0.05;
                    else if (hedge->name() == Seldom().name()) plusHedge += 0.01;
                    else if (hedge->name() == Any().name()) plusHedge += 0.99;
                    else plusHedge = fl::nan; //Unreconized hedge combination (e.g. Any)
                }

                break;
            }

			int term = 0;

            if (not fl::Op::isNan(plusHedge)) {
                term = termIndexPlusOne + plusHedge;
            } else {
				throw fl::Exception("[exporter error] "
                    "RuleMatrixConvert: unreconized hedge combination.", FL_AT);
            }

			rulePart.push_back((negated < 0)? -term: term);
        }

        return rulePart;
    }

	void RuleMatrixConvert::fromMatrix(RuleBlock* ruleBlock, const Engine* engine, std::vector<RuleVector> ruleMatrix) const {
		// delete any existing rules in the RuleBlock
		while(ruleBlock->numberOfRules()) {
			delete ruleBlock->removeRule(ruleBlock->numberOfRules() - 1);
		}

		for(unsigned int i = 0; i < ruleMatrix.size(); i++) {
			RuleVector& ruleVector = ruleMatrix[i];

            if ((int) ruleVector.antecedent.size() != engine->numberOfInputVariables()) {
                std::ostringstream ss;
                ss << "[syntax error] expected <" << engine->numberOfInputVariables() << ">"
                        " input variables, but found <" << ruleVector.antecedent.size() << ">"
                        " input variables in rule <" << i << ">";
                throw fl::Exception(ss.str(), FL_AT);
            }
            if ((int) ruleVector.consequent.size() != engine->numberOfOutputVariables()) {
                std::ostringstream ss;
                ss << "[syntax error] expected <" << engine->numberOfOutputVariables() << ">"
                        " output variables, but found <" << ruleVector.consequent.size() << ">"
                        " output variables in rule <" << i << ">";
                throw fl::Exception(ss.str(), FL_AT);
            }

            std::vector<std::string> antecedent, consequent;

            for (std::size_t i = 0; i < ruleVector.antecedent.size(); ++i) {
                scalar inputCode = ruleVector.antecedent[i];
                if (fl::Op::isEq(inputCode, 0.0)) continue;
                std::ostringstream ss;
                ss << engine->getInputVariable(i)->getName() << " "
                        << fl::Rule::FL_IS << " "
                        << translateProposition(inputCode, engine->getInputVariable(i));
                antecedent.push_back(ss.str());
            }

            for (std::size_t i = 0; i < ruleVector.consequent.size(); ++i) {
                scalar outputCode = ruleVector.consequent[i];
                if (fl::Op::isEq(outputCode, 0.0)) continue;
                std::ostringstream ss;
                ss << engine->getOutputVariable(i)->getName() << " "
                        << fl::Rule::FL_IS << " "
                        << translateProposition(outputCode, engine->getOutputVariable(i));
                consequent.push_back(ss.str());
            }

            std::ostringstream rule;

            rule << fl::Rule::FL_IF << " ";
            for (std::size_t i = 0; i < antecedent.size(); ++i) {
                rule << antecedent.at(i);
                if (i + 1 < antecedent.size()) {
                    rule << " ";
                    if (ruleVector.op == 1) rule << fl::Rule::FL_AND << " ";
                    else if (ruleVector.op == 2) rule << fl::Rule::FL_OR << " ";
                    else throw fl::Exception("[syntax error] connector <"
                            + Op::str(ruleVector.op) + "> not recognized", FL_AT);
                }
            }

            rule << " " << fl::Rule::FL_THEN << " ";
            for (std::size_t i = 0; i < consequent.size(); ++i) {
                rule << consequent.at(i);
                if (i + 1 < consequent.size()) {
                    rule << " " << fl::Rule::FL_AND << " ";
                }
            }

            scalar weight = ruleVector.weight;
            if (not fl::Op::isEq(weight, 1.0))
                rule << " " << fl::Rule::FL_WITH << " " << Op::str(weight);

            ruleBlock->addRule(fl::Rule::parse(rule.str(), engine));
        }
	}

    std::string RuleMatrixConvert::translateProposition(scalar code, Variable* variable) const {
        int intPart = (int) std::floor(std::fabs(code)) - 1;
        scalar fracPart = std::fmod(std::fabs(code), 1.0);
        if (intPart >= variable->numberOfTerms()) {
            std::ostringstream ex;
            ex << "[syntax error] the code <" << code << "> refers to a term "
                    "out of range from variable <" << variable->toString() << ">";
            throw fl::Exception(ex.str(), FL_AT);
        }

        bool isAny = intPart < 0;
        std::ostringstream ss;
        if (code < 0) ss << Not().name() << " ";
        if (fl::Op::isEq(fracPart, 0.01)) ss << Seldom().name() << " ";
        else if (fl::Op::isEq(fracPart, 0.05)) ss << Somewhat().name() << " ";
        else if (fl::Op::isEq(fracPart, 0.2)) ss << Very().name() << " ";
        else if (fl::Op::isEq(fracPart, 0.3)) ss << Extremely().name() << " ";
        else if (fl::Op::isEq(fracPart, 0.4)) ss << Very().name() << " " << Very().name() << " ";
        else if (fl::Op::isEq(fracPart, 0.99)) ss << Any().name() << " ";
        else if (not fl::Op::isEq(fracPart, 0))
            throw fl::Exception("[syntax error] no hedge defined in FIS format for <"
                + fl::Op::str(fracPart) + ">", FL_AT);
        if (not isAny) {
            ss << variable->getTerm(intPart)->getName();
        }
        return ss.str();
    }

} // namespace fl
